
import * as PIXI from 'pixi.js';
import Game from './Game';

const pixiApp = new PIXI.Application({
  width: 1024,
  height: 768,
  backgroundColor: 0x000000 
});

const game = new Game(pixiApp)

document.body.appendChild(pixiApp.view)

document.addEventListener('keydown', (key) => {
  game.keyboardProcessor.onKeyDown(key)
})

document.addEventListener('keyup', (key) => {
  game.keyboardProcessor.onKeyUp(key)
})

pixiApp.ticker.add(game.update, game);