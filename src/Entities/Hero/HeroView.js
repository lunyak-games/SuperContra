import { Container, Graphics } from "pixi.js";

export class HeroView extends Container {
  _bounds = {
    height: 0,
    width: 0,
  };

  _collisionBox = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  };

  _stm = {
    currentState: "default",
    states: {},
  };

  _rootNode;

  constructor() {
    super();

    this._createNodeStructure();

    this._rootNode.pivot.x = 10;
    this._rootNode.x = 10;
    this._rootNode.scale.x *= 1;

    this._bounds.width = 20;
    this._bounds.height = 90;
    this.collisionBox.width = this._bounds.width;
    this.collisionBox.height = this._bounds.height;

    this._stm.states.stay = this._getStayImage();
    this._stm.states.stayUp = this._getStayUpImage();
    this._stm.states.run = this._getRunImage();
    this._stm.states.runUp = this._getRunUpImage();
    this._stm.states.runDown = this._getRunDownImage();
    this._stm.states.lay = this._getLayImage();
    this._stm.states.jump = this._getJumpImage();
    this._stm.states.fall = this._getFallImage();

    for (let key in this._stm.states) {
      this._rootNode.addChild(this._stm.states[key]);
    }

    this._toState("fall");

    this.addChild(this._rootNode);
  }

  get collisionBox() {
    this._collisionBox.x = this.x;
    this._collisionBox.y = this.y;

    return this._collisionBox;
  }

  showStay() {
    this._toState("stay");
  }

  showStayUp() {
    this._toState("stayUp");
  }

  showRun() {
    this._toState("run");
  }

  showRunUp() {
    this._toState("runUp");
  }

  showRunDown() {
    this._toState("runDown");
  }

  showLay() {
    this._toState("lay");
  }

  showJump() {
    this._toState("jump");
  }

  showFall() {
    this._toState("fall");
  }

  flip(direction) {
    switch (direction) {
      case 1:
      case -1:
        this._rootNode.scale.x = direction;
    }
  }

  _toState(key) {
    if (this._stm.currentState === key) {
      return;
    }

    for (let key in this._stm.states) {
      this._stm.states[key].visible = false;
    }

    this._stm.states[key].visible = true;
    this._stm.currentState = key;
  }

  _createNodeStructure() {
    const rootNode = new Container();
    this.addChild(rootNode);
    this._rootNode = rootNode;
  }

  _getStayImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.drawRect(0, 30, 70, 5);

    return view;
  }

  _getStayUpImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.drawRect(8, -40, 5, 40);

    return view;
  }

  _getRunImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.drawRect(0, 30, 70, 5);
    view.transform.skew.x = -0.1;

    return view;
  }

  _getRunImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.drawRect(0, 30, 70, 5);
    view.transform.skew.x = -0.1;

    return view;
  }

  _getRunUpImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.lineTo(0, 30);
    view.lineTo(40, -20);
    view.lineTo(45, -15);
    view.lineTo(0, 40);
    view.transform.skew.x = -0.1;

    return view;
  }

  _getRunDownImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.lineTo(0, 20);
    view.lineTo(40, 60);
    view.lineTo(35, 65);
    view.lineTo(0, 30);
    view.transform.skew.x = -0.1;

    return view;
  }

  _getLayImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 90, 20);
    view.drawRect(90, 0, 40, 5);

    view.x -= 45;
    view.y += 70;

    return view;
  }

  _getJumpImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 40, 40);

    view.x -= 10;
    view.y += 25;

    return view;
  }

  _getFallImage() {
    const view = new Graphics();
    view.lineStyle(2, 0xffff00);
    view.drawRect(0, 0, 20, 90);
    view.drawRect(10, 20, 5, 60);

    view.transform.skew.x = -0.1;

    return view;
  }
}
