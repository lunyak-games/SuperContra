import Box from "./Box";
import Platform from "./Platform";

export default class PlatformFactory {
    _pixiApp
  constructor(pixiApp) {
    this._pixiApp = pixiApp;
  }

  createPlatform(x, y) {
    const platform = new Platform();
    platform.x = x;
    platform.y = y;
    this._pixiApp.stage.addChild(platform);

    return platform;
  }

  createBox(x, y) {
    const box = new Box();
    box.x = x;
    box.y = y;
    this._pixiApp.stage.addChild(box);

    return box;
  }
}
