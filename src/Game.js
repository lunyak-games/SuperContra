import Hero from "./Entities/Hero/Hero";
import PlatformFactory from "./Entities/Platforms";
import KeyBoardProcessor from "./KeyBoardProcessor";

export default class Game {
  _pixiApp;
  _hero;
  _platforms = [];

  keyboardProcessor;

  constructor(pixiApp) {
    this._pixiApp = pixiApp;

    this._hero = new Hero(this._pixiApp.stage);
    this._hero.x = 100;
    this._hero.y = 100;

    const platform = new PlatformFactory(this._pixiApp);

    this._platforms.push(platform.createPlatform(100, 400));
    this._platforms.push(platform.createPlatform(300, 400));
    this._platforms.push(platform.createPlatform(500, 400));
    this._platforms.push(platform.createPlatform(700, 400));
    this._platforms.push(platform.createPlatform(900, 400));

    this._platforms.push(platform.createPlatform(300, 550));

    this._platforms.push(platform.createBox(0, 738));
    this._platforms.push(platform.createBox(200, 738));

    const box = platform.createBox(400, 708);
    box.isStep = true;
    this._platforms.push(box);

    // ----------------------------------------------------------------

    this.keyboardProcessor = new KeyBoardProcessor(this);
    this.setKeys();
  }

  update() {
    const prevPoint = {
      x: this._hero.x,
      y: this._hero.y,
    };

    this._hero.update();

    for (let i = 0; i < this._platforms.length; i++) {
      if (this._hero.isJumpState() && this._platforms[i].type !== "box") {
        continue;
      }

      const collisionResult = this.getPlatformCollisionResult(
        this._hero,
        this._platforms[i],
        prevPoint
      );

      if (collisionResult.vertical == true) {
        this._hero.stay(this._platforms[i].y);
      }
    }
  }

  getPlatformCollisionResult(character, platform, prevPoint) {
    const collisionResult = this.getOrientCollisionResult(
      character.collisionBox,
      platform,
      prevPoint
    );

    if (collisionResult.vertical == true) {
      character.y = prevPoint.y;
    }

    if (collisionResult.horizontal == true && platform.type === "box") {
      if (platform.isStep) {
        character.stay(platform.y);
      }
      character.x = prevPoint.x;
    }

    return collisionResult;
  }

  getOrientCollisionResult(aaRectangle, bbRectangle, aaPrevPoint) {
    const collisionResult = {
      horizontal: false,
      vertical: false,
    };

    if (!this.isCheckAABB(aaRectangle, bbRectangle)) {
      return collisionResult;
    }

    aaRectangle.y = aaPrevPoint.y;

    if (!this.isCheckAABB(aaRectangle, bbRectangle)) {
      collisionResult.vertical = true;
      return collisionResult;
    }

    collisionResult.horizontal = true;
    return collisionResult;
  }

  isCheckAABB(entity, area) {
    return (
      entity.x < area.x + area.width &&
      entity.x + entity.width > area.x &&
      entity.y < area.y + area.height &&
      entity.y + entity.height > area.y
    );
  }

  setKeys() {
    this.keyboardProcessor.getButton("KeyS").executeDown = function () {
      if (
        (this.keyboardProcessor.isButtonPressed("ArrowDown") &&
          !this.keyboardProcessor.isButtonPressed("ArrowLeft")) ||
        this.keyboardProcessor.isButtonPressed("ArrowRight")
      ) {
        this._hero.trowDown();
      } else {
        this._hero.jump();
      }
    };

    // arrowLeft
    const arrowLeft = this.keyboardProcessor.getButton("ArrowLeft");
    arrowLeft.executeDown = function () {
      this._hero.startLeftMove();
      this._hero.setView(this.getArrowsButtonContext());
    };
    arrowLeft.executeUp = function () {
      this._hero.stopLeftMove();
      this._hero.setView(this.getArrowsButtonContext());
    };
    // rightMove
    // arrowRight
    const arrowRight = this.keyboardProcessor.getButton("ArrowRight");
    arrowRight.executeDown = function () {
      this._hero.startRightMove();
      this._hero.setView(this.getArrowsButtonContext());
    };
    arrowRight.executeUp = function () {
      this._hero.stopRightMove();
      this._hero.setView(this.getArrowsButtonContext());
    };

    // arrowUp
    const arrowUp = this.keyboardProcessor.getButton("ArrowUp");
    arrowUp.executeDown = function () {
      this._hero.setView(this.getArrowsButtonContext());
    };
    arrowUp.executeUp = function () {
      this._hero.setView(this.getArrowsButtonContext());
    };

    // arrowDown
    const arrowDown = this.keyboardProcessor.getButton("ArrowDown");
    arrowDown.executeDown = function () {
      this._hero.setView(this.getArrowsButtonContext());
    };
    arrowDown.executeUp = function () {
      this._hero.setView(this.getArrowsButtonContext());
    };
  }

  getArrowsButtonContext() {
    const buttonContext = {};
    buttonContext.arrowLeft =
      this.keyboardProcessor.isButtonPressed("ArrowLeft");
    buttonContext.arrowRight =
      this.keyboardProcessor.isButtonPressed("ArrowRight");
    buttonContext.arrowUp = this.keyboardProcessor.isButtonPressed("ArrowUp");
    buttonContext.arrowDown =
      this.keyboardProcessor.isButtonPressed("ArrowDown");

    return buttonContext;
  }
}
