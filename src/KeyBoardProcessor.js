export default class KeyBoardProcessor {
  _keyMap = {
    KeyS: {
      isDown: false,
    },
    KeyA: {
      isDown: false,
    },
    ArrowLeft: {
      isDown: false,
    },
    ArrowRight: {
      isDown: false,
    },
    ArrowUp: {
      isDown: false,
    },
    ArrowDown: {
      isDown: false,
    },
  };

  _gameContext;

  constructor(gameContext) {
    this._gameContext = gameContext;
  }

  getButton(key) {
    return this._keyMap[key];
  }

  onKeyDown(key) {
    const button = this._keyMap[key.code];

    if (button != undefined) {
      button.isDown = true;
      if (button.hasOwnProperty("executeDown")) {
        button.executeDown.call(this._gameContext);
      }
    }
  }

  onKeyUp(key) {
    const button = this._keyMap[key.code];
    if (button != undefined) {
      button.isDown = false;
      if (button.hasOwnProperty("executeUp")) {
        button.executeUp.call(this._gameContext);
      }
    }
  }

  isButtonPressed(key) {
    return this._keyMap[key].isDown;
  }
}
