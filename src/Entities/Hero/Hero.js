import { HeroView } from "./HeroView";

const States = {
  Stay: "stay",
  Jump: "jump",
  FlyDown: "flyDown",
};

export default class Hero {
  _GRAVITY_FORCE = 0.2;
  _SPEED = 3;
  _JUMP_FORCE = 9;
  _velocityX = 0;
  _velocityY = 0;

  _movement = {
    x: 0,
    y: 0,
  };

  _directionContext = {
    left: 0,
    right: 0,
  };

  _state = States.Stay;

  _view;

  _isLay = false;
  _isStayUp = false;

  constructor(stage) {
    this._view = new HeroView();
    stage.addChild(this._view);

    this._state = States.Jump;
    this._view.showJump();
  }

  get collisionBox() {
    return this._view.collisionBox;
  }

  get x() {
    return this._view.x;
  }
  set x(value) {
    this._view.x = value;
  }
  get y() {
    return this._view.y;
  }
  set y(value) {
    return (this._view.y = value);
  }

  update() {
    this._velocityX = this._movement.x * this._SPEED;
    this.x += this._velocityX;

    if (this._velocityY > 0) {
      if (this._state == States.Jump || this._state == States.FlyDown) {
        this._view.showFall();
      }
      this._state = States.FlyDown;
    }

    this._velocityY += this._GRAVITY_FORCE;
    this.y += this._velocityY;
  }

  stay(platformY) {
    if (this._state == States.Jump || this._state == States.FlyDown) {
      const fakeButtonContext = {};
      fakeButtonContext.arrowLeft = this._movement.x == -1;
      fakeButtonContext.arrowRight = this._movement.x == 1;
      fakeButtonContext.arrowDown = this._isLay;
      fakeButtonContext.arrowUp = this._isStayUp;
      this._state = States.Stay;
      this.setView(fakeButtonContext);
    }

    this._state = States.Stay;
    this._velocityY = 0;

    this.y = platformY - this._view.collisionBox.height;
  }

  jump() {
    if (this._state == States.Jump || this._state == States.FlyDown) {
      return;
    }
    this._state = States.Jump;
    this._velocityY -= this._JUMP_FORCE;
    this._view.showJump();
  }

  isJumpState() {
    return this._state == States.Jump;
  }

  throwDown() {
    this._state = States.Jump;
    this._view.showFall();
  }

  startLeftMove() {
    this._directionContext.left = -1;

    if (this._directionContext.right > 0) {
      this._movement.x = 0;
      return;
    }

    this._movement.x = -1;
  }

  stopLeftMove() {
    this._directionContext.left = 0;
    this._movement.x = this._directionContext.right;
  }

  startRightMove() {
    this._directionContext.right = 1;

    if (this._directionContext.left < 0) {
      this._movement.x = 0;
      return;
    }

    this._movement.x = 1;
  }

  stopRightMove() {
    this._directionContext.right = 0;
    this._movement.x = this._directionContext.left;
  }

  setView(buttonContext) {
    this._view.flip(this._movement.x);
    this._isLay = buttonContext.arrowDown;
    this._isStayUp = buttonContext.arrowUp;

    if (this._state === States.Jump || this._state === States.FlyDown) {
      return;
    }

    if (buttonContext.arrowLeft || buttonContext.arrowRight) {
      if (buttonContext.arrowUp) {
        this._view.showRunUp();
      } else if (buttonContext.arrowDown) {
        this._view.showRunDown();
      } else {
        this._view.showRun();
      }
    } else {
      if (buttonContext.arrowUp) {
        this._view.showStayUp();
      } else if (buttonContext.arrowDown) {
        this._view.showLay();
      } else {
        this._view.showStay();
      }
    }
  }
}
